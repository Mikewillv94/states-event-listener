import "./App.css";
import React from "react";
import NumberList from "./components/NumberList";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberList: [],
      count: 10,
    };
  }

  addNumber = () => {
    this.setState({ count: this.state.count + 10 });
    this.setState({
      numberList: [...this.state.numberList, this.state.count],
    });
  };

  render() {
    console.log(this.state.numberList);
    return (
      <div className="App-header">
        <button onClick={this.addNumber}>Adicinar Número</button>
        <NumberList list={this.state.numberList}></NumberList>
      </div>
    );
  }
}

export default App;
