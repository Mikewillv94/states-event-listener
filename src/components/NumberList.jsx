import React from "react";
import Number from "./Number";

export default class NumberList extends React.Component {
  render() {
    return (
      <>
        {this.props.list.map((num, index) => (
          <Number key={index}>{num}</Number>
        ))}
      </>
    );
  }
}
