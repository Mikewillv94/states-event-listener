import React from "react";

export default class Number extends React.Component {
  render() {
    return (
      <>
        <div style={{ border: "solid 2px green" }}>{this.props.children}</div>
      </>
    );
  }
}
